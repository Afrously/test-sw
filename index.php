<?php
// connect to the database

include('mysql_connect.php');

// get results from database

$result = mysqli_query($conn,"SELECT * FROM products")

or die(mysqli_error());
?>


<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" >
             <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <h1>Product list</h1>
        <a href="new.html"><input type="submit" name="action" value="Add Product"></a><br>
        <a href="edit.php"><input type="submit" name="edit_action" value="Update"></a>
        <hr>
        <table style=" width:'50%'!important " class='table'>
            <tr bgcolor='#CCCCCC'>

                <th>SKU</th>
                <th>Name</th>
                <th>Price</th>
                <th>Type</th>
                <th>Details</th>
                <th>Update</th>
            </tr>

      <?php
      while ($res = mysqli_fetch_array($result)){

       echo "<tr>";
       echo "<td>".$res['sku_product']."</td>";
       echo "<td>".$res['name_product']."</td>";
       echo "<td>".$res['price_product']."</td>";
       echo "<td>".$res['type_product']."</td>";
       echo "<td>".$res['sizedvd_product']."".$res['weightbook_product']."".$res['heightfurn_product']." ".$res['widthfurn_product']." ".$res['lengthfurn_product']."</td>";
       echo "<td><a href=\"delete.php?id_product=$res[id_product]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";

      }

     ?>

            </table>
    </body>
</html>
