<?php
// connect to the database
include('mysql_connect.php');
// get results from database
$result = mysqli_query($conn,"SELECT * FROM products")
or die(mysqli_error());
?>
<html>
<head>
    <title>Update Data</title>
    <link rel="stylesheet"  href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" >
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
</head>
<body>
    <h1>Product list update</h1>
    <hr>
    <table style=" width:'50%'!important " class='table'>
        <tr bgcolor='#CCCCCC'>
            <th>SKU</th>
            <th>Name</th>
            <th>Price</th>
            <th>Type</th>
            <th>Details</th>
            <th></th>
        </tr>
        <?php while ($res = mysqli_fetch_array($result)) { ?>
        <tr>
            <form action="update.php" method="post">
                <td><input type="text" name="sku_p" value="<?php echo $res['sku_product']; ?>"></td>
                <td><input type="text" name="name_p" value="<?php echo $res['name_product']; ?>"></td>
                <td><input type="text" name="price_p" value="<?php echo $res['price_product']; ?>"></td>
                <td><input type="text" name="type_p" value="<?php echo $res['type_product']; ?>"></td>
                <td>
                    Size for DVD: <input type="text" name="sizedvd_p" value="<?php echo $res['sizedvd_product']; ?>"><br>
                    Weight for Book: <input type="text" name="weightbook_p" value="<?php echo $res['weightbook_product']; ?>"><br>
                    For Furniture<br>
                    H: <input type="text" name="heightfurn_p" value="<?php echo $res['heightfurn_product']; ?>"><br>
                    W: <input type="text" name="widthfurn_p" value="<?php echo $res['widthfurn_product']; ?>"><br>
                    L: <input type="text" name="lengthfurn_p" value="<?php echo $res['lengthfurn_product']; ?>"><br>
                </td>
                <input type="hidden" name="id_p" value="<?php echo $res['id_product']; ?>">
                <td><input type="submit" name="update" value="Submit">
            </form>
        </tr>
        <?php } ?>
        <a href="index.php">Home</a>
    </table>
</body>
</html>
