-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2018 at 01:41 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `product_list`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id_product` int(11) NOT NULL,
  `sku_product` varchar(255) DEFAULT NULL,
  `name_product` varchar(255) DEFAULT NULL,
  `price_product` varchar(255) DEFAULT NULL COMMENT 'Euro',
  `type_product` varchar(255) DEFAULT NULL,
  `sizedvd_product` varchar(120) DEFAULT NULL,
  `weightbook_product` varchar(120) DEFAULT NULL,
  `heightfurn_product` varchar(120) DEFAULT NULL,
  `widthfurn_product` varchar(120) DEFAULT NULL,
  `lengthfurn_product` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id_product`, `sku_product`, `name_product`, `price_product`, `type_product`, `sizedvd_product`, `weightbook_product`, `heightfurn_product`, `widthfurn_product`, `lengthfurn_product`) VALUES
(14, '90ZO0', 'Fantasy', '90', 'Book', '', '43KG', '', '', ''),
(16, '92394299', 'unicorn', '90', 'Furniture', '', '', '10', '20', '30'),
(17, '3342', 'DvD', '90euro', 'DvD', '635MB', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_product`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
